package com.example.abookforyou

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.example.abookforyou.recycler.Adapter
import com.example.abookforyou.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    
    private lateinit var adapter : Adapter
    private val viewModel by lazy { ViewModelProviders.of(this).get(MainViewModel::class.java)}
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter = Adapter(this)
        recycle.layoutManager = GridLayoutManager(this,2)
        recycle.adapter = adapter
        observerData()
    }

       fun observerData(){
           viewModel.fetchUserData().observe(this,Observer{
               adapter.setListdata(it)
               adapter.notifyDataSetChanged()
           })
       }

}
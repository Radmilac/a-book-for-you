package com.example.abookforyou.book_format

import android.os.Parcel
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class Book(val imageUrl:String? = "URL IMmage",
                val name:String? = "Naziv knjige",
                val writer:String? = "Pisac knjige",
                val des:String? ="Opis knjige" ) : Parcelable {

    constructor (parcel: Parcel) : this (
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString())  {

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(imageUrl)
        parcel.writeString(name)
        parcel.writeString(writer)
        parcel.writeString(des)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Book>{
        override fun createFromParcel(parcel: Parcel): Book {
            return Book(parcel)
        }

        override fun newArray(size: Int): Array<Book?> {
            return arrayOfNulls(size)
        }
    }
    }




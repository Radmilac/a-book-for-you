package com.example.abookforyou.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.abookforyou.book_format.Book
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.QueryDocumentSnapshot

class Repo {
    fun getUserData(): LiveData<MutableList<Book>> {
        val mutableData = MutableLiveData<MutableList<Book>>()
        FirebaseFirestore.getInstance().collection("Book").get().addOnSuccessListener { result->
            val listData = mutableListOf<Book>()
            for (document in result){
                val imageUrl = document.getString("imageUrl")
                val name = document.getString("name")
                val writer = document.getString("writer")
                val des = document.getString("des")
                val book = Book(imageUrl!!,name!!,writer!!,des!!)
                listData.add(book)
            }
            mutableData.value = listData
        }
        return mutableData
    }
}
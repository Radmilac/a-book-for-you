package com.example.abookforyou.viewmodel

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.abookforyou.book_format.Book

class MainViewModel :  ViewModel() {

    val repo = Repo()
    fun fetchUserData(): LiveData<MutableList<Book>> {
        val mutableData = MutableLiveData<MutableList<Book>>()
        repo.getUserData().observeForever{ userList->
            mutableData.value = userList
        }
        return mutableData
    }
}
package com.example.abookforyou.description

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.example.abookforyou.R
import com.example.abookforyou.book_format.Book
import kotlinx.android.synthetic.main.activity_book_description.*

class BookDescription : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_description)

        var books = intent.getParcelableExtra("jbg") as? Book

        Glide.with(this).load(books?.imageUrl!!).into(bookImg1)
        nameTxt1.text = books?.name!!
        autorTxt1.text = books?.writer!!
        descriptionTxt.text = books?.des!!
    }
}
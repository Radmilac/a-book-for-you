package com.example.abookforyou.recycler

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.abookforyou.R
import com.example.abookforyou.book_format.Book
import com.example.abookforyou.description.BookDescription
import kotlinx.android.synthetic.main.activity_book_description.view.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.book_format.view.*
import java.util.*

class Adapter(private val context: Context) : RecyclerView.Adapter<Adapter.Holder>() {

    private var datalist = mutableListOf<Book>()
    fun setListdata(data: MutableList<Book>){
        datalist = data
    }

    inner class Holder(itemView : View) : RecyclerView.ViewHolder(itemView){
         fun bindView(book: Book){
             Glide.with(context).load(book.imageUrl).into(itemView.bookImg)
             itemView.nameTxt.text = book.name
             itemView.autorTxt.text= book.writer


             itemView.bookImg.setOnClickListener(
                     View.OnClickListener {
                         val intent = Intent(context, BookDescription::class.java)
                         intent.putExtra("jbg", book)
                         context.startActivity( intent)
                     }
             )
         }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(context).inflate(R.layout.book_format, parent, false )
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
           val book = datalist[position]
           holder.bindView(book)
    }

    override fun getItemCount(): Int {
       return if (datalist.size> 0){
            datalist.size
        }else{
            0
        }
    }
}